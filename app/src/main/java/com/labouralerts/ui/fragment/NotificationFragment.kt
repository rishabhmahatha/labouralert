package com.labouralerts.ui.fragment

import android.view.View
import com.labouralerts.R

class NotificationFragment : BaseFragments() {
    override fun defineLayoutResource(): Int {
        return R.layout.fragment_notification
    }

    override fun initializeComponent(view: View) {

    }

}