package com.labouralerts.ui.fragment

import android.view.View
import com.labouralerts.R

class Top10Fragment : BaseFragments() {
    override fun defineLayoutResource(): Int {
        return R.layout.fragment_top_10
    }

    override fun initializeComponent(view: View) {

    }

}