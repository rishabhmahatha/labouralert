package com.labouralerts.utils

object Constants {
    const val MAX_CLICK_INTERVAL: Long = 500
}